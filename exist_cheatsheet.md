# eXist-DB Cheat Sheet
## Note

- Der Heimatpfad zu eXist-DB wird durch `$EXIST` bezeichnet und hängt von dem jeweiligen Rechner ab, auf dem eXist-DB installiert wurde.

## Java-Client
`$EXIST/bin/client.sh`

## eXist-DB starten und runterfahren
### Starten
`$EXIST\bin\startup.bat		# In DOS|Windows`
`$EXIST/bin/startup.sh		# In Linux`
`$EXIST/bin/startup.sh & 	# im Hintergrund starten`

### Runterfahren
`$EXIST\bin\shutdown.bat							# In DOS|Windows`
`$EXIST/bin/shutdown.sh							# In Linux`
`$EXIST/bin/shutdown.sh	-p [Adminpasswort]		# Falls ein Adminpasswort vergeben wurde`

## eXist Dashboard
`http://localhost:8080/exist/apps/dashboard/index.html`

## Datenmanagement auf der Kommandozeile
### Collection erstellen 
`$EXIST/bin/client.sh -m [pfad/der/zu/erstellenden/collection]`

### Datei hochladen (in existierende Collection)
`$EXIST/bin/client.sh -c [pfad/der/zielcollection] -p [pfad/auf/lokalem/rechner]`

### Datei hochladen (in nicht existierende Collection)
```
$EXIST/bin/client.sh -m [pfad/der/zu/erstellenden/collection]
$EXIST/bin/client.sh -c [pfad/der/collection] -p [pfad/auf/lokalem/rechner]
```

### Rekursiver Ordner Upload
`$EXIST/bin/client.sh -d -c [pfad/einer/collection] -p [pfad/auf/lokalem/rechner]`

### Dateien/Collections löschen
`$EXIST/bin/client.sh -c [pfad/zur/collection] -r [Dateiname]`
`$EXIST/bin/client.sh -c [pfad/zum/Elternverzeichnis] -R [name der collection]`
