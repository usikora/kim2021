xquery version "3.1";

module namespace convert="https://dini.de/ag/kim/workshop2021/exist/kim/convert";
import module namespace kim="https://dini.de/ag/kim/workshop2021/exist/kim" at "kim.xqm";
import module namespace kwic="http://exist-db.org/xquery/kwic";

declare namespace mods="http://www.loc.gov/mods/v3";
declare namespace mets="http://www.loc.gov/METS/";
declare namespace tei="http://www.tei-c.org/ns/1.0";



declare function convert:nodes
    ( $nodes ){
        for $node in $nodes
        return 
            typeswitch($node)
                case text() return $node
                case comment() return $node
                case element(mods:title) return kwic:summarize($node, <config width="40"/>)
                
                case element(tei:choice) return 
                    if ( $node[tei:expan] )
                    then( convert:nodes($node/tei:expan) )
                    else( convert:nodes($node/tei:abbr) )
                    
                case element( tei:div ) return (
                    element div {
                        if ( data($node/@type) = ("act", "scene") )
                        then( 
                            attribute class { $node/@type => data() },
                            convert:nodes( $node/node() )
                        )
                        else(
                            convert:nodes( $node/node() )
                        )
                    }
                )
                
                case element( tei:head ) return (
                    if ( $node[not(@type)])
                    then( <h3 class="title">{ $node => data() }</h3> )
                    else()
                )
                
                case element(tei:l) return (
                        <div class="line-mark">{ data($node/@n) }</div>,
                        convert:nodes( $node/node() )
                    )
                
                case element( tei:sp ) return <div class="sp">{ convert:nodes($node/node()) }</div>
                
                case element( tei:speaker ) return <span class="speaker-name">{ convert:pers-name($node) }</span>
                
                case element( tei:stage ) return <p class="stage">{ $node => data() }</p>
                
                case element( tei:teiHeader ) return ()
                
                case element( exist:match ) return element span { attribute{"class"}{"hi"}, convert:nodes($node/node())}
                
                default return convert:nodes( $node/node() )
};


declare function convert:pers-name( $speaker-node ){
    let $person := $speaker-node/root()//tei:person[@xml:id = translate($speaker-node/parent::tei:sp/@who, "#", "")]
    return $person/tei:persName[@type = "standard"] => data()
};
