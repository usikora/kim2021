xquery version "3.1";

module namespace kim="https://dini.de/ag/kim/workshop2021/exist/kim";

import module namespace config="https://dini.de/ag/kim/workshop2021/exist/config" at "../config.xqm";

declare namespace mods="http://www.loc.gov/mods/v3";
declare namespace mets="http://www.loc.gov/METS/";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace dcterms="http://purl.org/dc/terms/";
declare namespace oai="http://www.openarchives.org/OAI/2.0/";

(: ######################################################
 :      Variabeln 
 : ######################################################:)

declare variable $kim:resources := collection( $config:data-root );



(: ######################################################
 :      Funktionen 
 : ######################################################:)

(:~  
 : liefert alle METS Ressourcen aus.
 :
 : @return mets:mets Ressourcen als sequence
 : @version 0.1 (2021-03-16)
 : @author Uwe Sikora
 :)
declare function kim:mets-files
    (){
    collection($config:data-root || "/metsmods")//mets:mets
};


(:~  
 : liefert alle TEI Ressourcen aus.
 : 
 : @return tei:TEI Ressourcen als sequence
 : @version 0.1 (2021-03-16)
 : @author Uwe Sikora
 :)
declare function kim:tei-files
    (){
    collection($config:data-root || "/tei")//mets:mets
};

declare function kim:tei-files
    ( $name ){
    let $files := collection($config:data-root || "/tei")
    return $files[ tokenize(base-uri(), "/")[last()] = $name ]
};


(:~  
 : liefert alle Ressourcen eines festgelegten Typs aus, wenn spezifiziert. 
 :
 : @param $type type der Ressourcen, die ausgeliefert sollen.
 : @return sequence mit Ressourcen
 : 
 : @version 0.1 (2021-03-16)
 : @author Uwe Sikora
 :)
declare function kim:get-resources
    ( $type as xs:string? ){
    if ( $type and $type = "tei" ) then (
        kim:tei-files()
    ) else if ( $type and $type = "mets" ) then (
        kim:mets-files()
    ) else (
        $kim:resources
    )
};


(:~  
 : Liefert eine verallgemeinerte Metadaten Beschreibung für alle Ressourcen aus. 
 :
 : @return sequence mit metadata Elementen
 : @version 0.1 (2021-03-16)
 : @author Uwe Sikora
 :)
declare function kim:get-resources-metadata
    (){
    kim:get-resources-metadata( $kim:resources )
};


(:~  
 : ## ! Mehrfachdeklaration von Funktionen (function overloading) ist möglich ! ##
 : Liefert eine verallgemeinerte Metadaten Beschreibung für alle Ressourcen aus. 
 :
 : @param $resources Die Ressourcen, deren Metadaten ausgeliefert werden sollen.
 : @return sequence mit metadata Elementen
 : @version 0.1 (2021-03-16)
 : @author Uwe Sikora
 :)
declare function kim:get-resources-metadata
    ( $resources ){
    for $resource in $resources
    let $root := $resource/root()
    return 
            if( $root/node()[self::tei:TEI] ) 
            then kim:tei-metadata($resource)
            else kim:mets-metadata($resource)
};


(:~
 : Erzeugt eine verallgemeinerte Metadaten Beschreibung mit dcterms für eine METS Ressource. 
 :
 : @param $resources Eine METS Ressource.
 : @return metadata Element
 : @version 0.1 (2021-03-16)
 : @author Uwe Sikora
 :)
declare function kim:mets-metadata
    ( $resource ){
    let $uri := $resource/base-uri()
    let $file := kim:resource-uri($uri)
    let $metadata-section := $resource//mets:dmdSec[1]
    let $title-info := $metadata-section//mods:titleInfo[1]
    let $title := data($title-info/mods:title)
    let $subtitle := data($title-info/mods:subTitle)
    let $location := data($metadata-section//mods:location[1]/mods:physicalLocation)
    let $class := data($metadata-section//mods:classification[1])
    let $elements-count := count( kim:get-resource-element($resource) )
    return 
        <metadata xmlns:dcterms="http://purl.org/dc/terms/" uri="{$uri}" file="{$file}" elements="{$elements-count}" type="METS">
            <dcterms:title>{ if ($subtitle) then ($title || ": "||$subtitle) else ($title) }</dcterms:title>
            <dcterms:spatial>
                <dcterms:Location>{$location}</dcterms:Location>
            </dcterms:spatial>
            <dcterms:type>{$class}</dcterms:type>
            <dcterms:date>{data($metadata-section//mods:dateIssued)}</dcterms:date>
        </metadata>
};


(:~
 : Erzeugt eine verallgemeinerte Metadaten Beschreibung mit dcterms für eine TEI Ressource. 
 :
 : @param $resources Eine METS Ressource.
 : @return metadata Element
 : @version 0.1 (2021-03-16)
 : @author Uwe Sikora
 :)
declare function kim:tei-metadata( $resource ){
    let $uri := $resource/base-uri()
    let $file := kim:resource-uri($uri)
    let $metadata-section := $resource//tei:teiHeader
    let $title-info := $metadata-section//tei:titleStmt
    let $title := data($title-info/tei:title[@type = "statement"])
    let $author := data($title-info/tei:author)
    let $subtitle := ()
    let $elements-count := count( kim:get-resource-element($resource) )
    return 
        <metadata xmlns:dcterms="http://purl.org/dc/terms/" uri="{$uri}" file="{$file}" elements="{$elements-count}" type="TEI">
            <dcterms:title>{$title}</dcterms:title>
            <dcterms:creator>
                <dcterms:Agent>{$author}</dcterms:Agent>
            </dcterms:creator>
        </metadata>
};


(:~
 : Liefert alle Elemente einer Ressource zurück. 
 :
 : @param $resources Eine Ressource.
 : @return Sequence aller Element Knoten der Ressource
 : @version 0.1 (2021-03-16)
 : @author Uwe Sikora
 :)
declare function kim:get-resource-element( $resource ){
    $resource//element()
};


(:~
 : Erzeugt einen Resource URI aus dem Base Uri einer Ressource zum Download einer Ressource. 
 :
 : @param $base-uri Der base-uri einer Ressource.
 : @return Ein Ressourcen URI der Form "/exist/apps/kim/data/[Dateiname]"
 : @version 0.1 (2021-03-16)
 : @author Uwe Sikora
 :)
declare function kim:resource-uri( $base-uri ){
    "/exist"||replace($base-uri, "^/db", "")
};


(:~
 : Erzeugt einen exide URL aus dem Base Uri einer Ressource zum Bearbeiten der entsprechenden Ressource in eXide. 
 :
 : @param $base-uri Der base-uri einer Ressource.
 : @return Ein Ressourcen URI der Form "/exist/apps/eXide/index.html?open=/db/apps/kim/data/[Dateiname]"
 : @version 0.1 (2021-03-16)
 : @author Uwe Sikora
 :)
declare function kim:exide-url( $base-uri ){
    "/exist/apps/eXide/index.html?open="|| $base-uri 
};