xquery version "3.1";

module namespace search="https://dini.de/ag/kim/workshop2021/exist/kim/search";
import module namespace kim="https://dini.de/ag/kim/workshop2021/exist/kim" at "kim.xqm";
import module namespace kwic="http://exist-db.org/xquery/kwic";

declare namespace mods="http://www.loc.gov/mods/v3";
declare namespace mets="http://www.loc.gov/METS/";
declare namespace tei="http://www.tei-c.org/ns/1.0";



declare function search:query
    ( $nodes as node()*, $query ) {
        for $hit in $nodes[ft:query(., $query)]
        order by ft:score($hit) descending
        return
            $hit
};

declare function search:search-and-expand
    ( $query ) {
        let $results := $nodes[ft:query(., $query)]
        let $expanded-results := for $result in $results return kwic:expand( $result )
        return $expanded-results
};



declare function local:convert-tei-hits
    ( $nodes ){
        for $node in $nodes
        return 
            typeswitch($node)
                case text() return $node
                case comment() return $node
                case element(tei:sp) return 
                    element div {
                        local:convert-tei-hits($node/node())
                    }
                
                case element(tei:speaker) return element span { attribute class {"speaker-name"}, local:convert-tei-hits($node/node())}
                case element(tei:choice) return 
                    if ( $node[tei:expan] )
                    then( local:convert-tei-hits($node/tei:expan) )
                    else( local:convert-tei-hits($node/tei:abbr) )
                
                case element(tei:l) return (
                        element span { attribute class {"line-mark"}, data($node/@n)},
                        local:convert-tei-hits($node/node())
                    )
                    
                case element(exist:match) return element span { attribute{"class"}{"hi"}, local:convert-tei-hits($node/node())}
                default return local:convert-tei-hits( $node/node() )
    };