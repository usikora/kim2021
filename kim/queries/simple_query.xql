xquery version "3.1";

import module namespace kim="https://dini.de/ag/kim/workshop2021/exist/kim" at "../modules/kim/kim.xqm";
import module namespace kwic="http://exist-db.org/xquery/kwic";
declare namespace mods="http://www.loc.gov/mods/v3";


(: kim:get-resources()//mods:title[ft:query(., 'Berge*')]:)

(:kim:get-resources()//node-name(xs:QName("http://www.loc.gov/mods/v3", "title")):)

(:for $hit in kim:get-resources()//mods:mods[ft:query(., 'location:Berlin')]:)
(:order by ft:score($hit) descending:)
(:return:)
(:    kwic:summarize($hit, <config width="40"/>):)

for $hit in kim:get-resources()//mods:mods[ft:query(., 'Berlin')]
order by ft:score($hit) descending
return
    $hit//mods:title