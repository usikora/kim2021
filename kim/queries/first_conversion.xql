xquery version "3.1";

import module namespace config="https://dini.de/ag/kim/workshop2021/exist/config" at "../modules/config.xqm";
import module namespace kim="https://dini.de/ag/kim/workshop2021/exist/kim" at "../modules/kim/kim.xqm";
import module namespace convert="https://dini.de/ag/kim/workshop2021/exist/kim/convert" at "../modules/kim/convert.xqm";

declare namespace mods="http://www.loc.gov/mods/v3";
declare namespace mets="http://www.loc.gov/METS/";
declare namespace tei="http://www.tei-c.org/ns/1.0";

kim:tei-files("shakespear_life_of_henry_v.xml")
(:let $tei-files := kim:tei-files():)
(:return convert:nodes($tei-files[1]//tei:text):)