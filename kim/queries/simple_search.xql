xquery version "3.1";

import module namespace kim="https://dini.de/ag/kim/workshop2021/exist/kim" at "../modules/kim/kim.xqm";
import module namespace search="https://dini.de/ag/kim/workshop2021/exist/kim/search" at "../modules/kim/search.xqm";
import module namespace kwic="http://exist-db.org/xquery/kwic";

declare namespace mods="http://www.loc.gov/mods/v3";
declare namespace mets="http://www.loc.gov/METS/";
declare namespace tei="http://www.tei-c.org/ns/1.0";

let $query := "'Henry'"
let $resources := $kim:resources//tei:text

(: # Verwendung des eXist-Volltextsuche Interfaces # :)

(:let $search := $resources[ft:query(., $query)]:)

(:for $hit in $search:)
(:let $score := ft:score($hit):)
(::)
(:order by $score descending:)
(::)
(:return $score:)



(: # Verwendung einer eigenen Search Funktion # :)

(:let $results := search:query($kim:resources//tei:text, $query):)

(:let $results := search:query($kim:resources//tei:div, $query):)

(:let $results := search:query($kim:resources//tei:sp, $query):)

(:return $results:)



(: # Zusammenfassen der Ergebnisse: Das KWIC (KeyWord-In-Context) Modul # :)

let $query := "'Queen'"
let $results := search:query($kim:resources//tei:sp, $query)

let $hits := for $hit in $results return kwic:summarize( $hit, <config width="100"/> )

let $hits := for $hit in $results return kwic:expand($hit)
let $summary := for $hit in $hits return kwic:get-summary( $hit, $hit//exist:match, <config width="100"/> )

return $summary





