xquery version "3.1";

import module namespace config="https://dini.de/ag/kim/workshop2021/exist/config" at "../modules/config.xqm";
import module namespace kim="https://dini.de/ag/kim/workshop2021/exist/kim" at "../modules/kim/kim.xqm";

declare namespace mods="http://www.loc.gov/mods/v3";
declare namespace mets="http://www.loc.gov/METS/";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace dcterms="http://purl.org/dc/terms/";


declare variable $local:resources := collection($config:data-root);

declare function local:mets-files(){
    $local:resources//mets:mets
};


declare function local:tei-files(){
    $local:resources//tei:TEI
};

(:$local:resources:)


let $mets-files := local:mets-files()

for $file in $mets-files
let $dmd-secs := $file/mets:dmdSec[mets:mdWrap//mods:location]
let $info := kim:mets-metadata($file)
return $info


(: TEI Retrieval 
 : ------------- 
 : :)

(:let $tei-texts : = local:tei-files():)
(::)
(:for $text in $tei-texts:)
(:let $play := $text//tei:div[@type = 'play']:)
(:let $acts := $play//tei:div[@type = "act"]:)
(:return kim:tei-metadata($text):)