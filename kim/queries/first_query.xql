xquery version "3.1";

import module namespace config="https://dini.de/ag/kim/workshop2021/exist/config" at "../modules/config.xqm";


declare namespace mods="http://www.loc.gov/mods/v3";
declare namespace mets="http://www.loc.gov/METS/";
declare namespace tei="http://www.tei-c.org/ns/1.0";


declare variable $local:resources := collection($config:data-root);

(:$local:resources:)


(: METS/MODS Retrieval 
 : ------------- 
 : :)

(:$local:resources//mets:mets:)

(:$local:resources//mets:mets:)

let $mets-files := $local:resources//mets:mets

for $file in $mets-files
let $dmd-secs := $file/mets:dmdSec[mets:mdWrap//mods:location]
return $dmd-secs


(: TEI Retrieval 
 : ------------- 
 : :)

(:$local:resources//tei:TEI:)

(:$local:resources//tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt:)

(:$local:resources//tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type = "statement"]:)


(:let $tei-texts : = $local:resources//tei:TEI//tei:text:)
(::)
(:for $text in $tei-texts:)
(:let $play := $text//tei:div[@type = 'play']:)
(:let $acts := $play//tei:div[@type = "act"]:)
(:return count($acts):)