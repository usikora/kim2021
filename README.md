# KIM 2021: eXist-DB Repository
Hier findet ihr alle Materialien, die wir für unseren eXist Workshop benötigen.

Herunterladen könnt ihr die Ressourcen entweder über einen herkömmlichen `Download` oder `git clone`. Beide Buttons befinden sich oben rechts, neben `Web IDE`.

## Daten
- Unter `data` habe ich ein paar beispielhafte Daten hinterlegt, die wir für unseren Workshop benutzen werden.

## eXist Anwendung
- Unter `kim` liegt eine kleine eXist Anwendung, die wir im Rahmen des Workshops zur Hilfe nehmen.

## IDE Kurzreferenzen zu XPath und XQuery
- Unter `ide_kurzreferenzen` liegen zwei PDFs, die als Cheatsheet für XPath und XQuery gedacht sind.

## eXist DB - Cheatsheet
- Nun ja, hier finden sich ein paar Komandozeilenbefehle zum schnellen nachschauen.
